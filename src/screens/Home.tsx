import React from 'react';
import {View,Text,Button,StyleSheet,BackHandler} from 'react-native'
// import Card from '../components/Card'
import Swiper from 'react-native-deck-swiper';
import Toast from 'react-native-simple-toast';
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#F5FCFF"
    },
    card: {
      flex: 1,
      borderRadius: 4,
      borderWidth: 2,
      borderColor: "#E8E8E8",
      justifyContent: "center",
      backgroundColor: "white"
    },
    text: {
      textAlign: "center",
      fontSize: 50,
      backgroundColor: "transparent"
    }
  });

const Home=({navigation})=>{

return(
    <View style={styles.container}>
    <Swiper
        cards={['DO', 'MORE', 'OF', 'WHAT', 'MAKES', 'YOU', 'HAPPY']}
        renderCard={(card) => {
            return (
                <View style={styles.card}>
                    <Text style={styles.text}>{card}</Text>
                </View>
            )
        }}
        onSwiped={(cardIndex) => {console.log(cardIndex)}}
        onSwipedAll={() => {
            Toast.show('there is no Card left!')
            navigation.navigate('SignIn')
        }}
        cardIndex={0}
        backgroundColor={'#4FD0E9'}
        stackSize= {3}>
        <Button
            onPress={() => {BackHandler.exitApp()}}
            title="Back">
           Back
        </Button>
    </Swiper>
</View>
)
}

export default Home