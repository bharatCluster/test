import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Pressable,
  TouchableOpacity,
  Button,
} from 'react-native';

import InputBox from '../components/InputBox';

import {getScreenHeight, getScreenWidth} from '../utils/Size';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {verticalScale} from 'react-native-size-matters';

const styles = StyleSheet.create({
  ImageBackground: {
    width: getScreenWidth(),
    height: getScreenHeight(),
  },
  textBox: {
    alignItems: 'center',
    marginTop: verticalScale(5),
  },
  text: {
    fontSize: 14,
  },
  textForgotBox: {
    alignItems: 'flex-end',
  },
});

const SignIn = ({navigation}) => {
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
 
  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('signup')
      return jsonValue != null ? JSON.parse(jsonValue) : null;

    } catch(e) {
      // error reading value
    }
  }
  const submit = async () => {
 

const data=await getData();
if(email==data.email && password==data.pass){
  navigation.navigate('Home')

}else{
  Toast.show('Incorrect credential!')
}
    
  
 
  }
  return (
    
     
<View >
  <View style={{alignItems:'center',marginVertical:10}}>
  <Text>
        Sign In Screen 
    </Text>
  </View>
    
      <InputBox
        placeholder="Email address"
        value={email}
        setValue={text => setemail(text.trim())}
      />
      <InputBox
        placeholder="Password"
        value={password}
        setValue={text => setpassword(text)}
        secureTextEntry
      />
      <Button onPress={submit} title="Sign In"></Button>
    </View>
     
  );
};

export default SignIn;
