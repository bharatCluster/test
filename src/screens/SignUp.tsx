import React, { useState} from 'react';
import {
  Text,
  View,
  StyleSheet,Button,

} from 'react-native';

import InputBox from '../components/InputBox';

import {getScreenHeight, getScreenWidth} from '../utils/Size';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {verticalScale} from 'react-native-size-matters';


const styles = StyleSheet.create({
  ImageBackground: {
    width: getScreenWidth(),
    height: getScreenHeight(),
  },
  textBox: {
    alignItems: 'center',
    marginTop: verticalScale(5),
  },
  text: {
    fontSize: 14,
  },
  textForgotBox: {
    alignItems: 'flex-end',
  },
});

const SignUp = ({navigation}) => {
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const [cpass, setcpass] = useState('');
  function ValidateEmail(mail:any) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return (true)
  }
 
    return (false)
}
function validatePass(pass:any){
  if(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{2,})/.test(pass)){
    return true
  }else{
    return false
  }
}
  const storeData = async () => {
    try {
      await AsyncStorage.setItem('signup', JSON.stringify({email:email,pass:password}))
      navigation.navigate('SignIn')
    } catch (e) {
      Toast.show('something went wrong!')
    }
  }
  const submit = async () => {
    if(ValidateEmail(email)){
     
      if(validatePass(password)){
        if(password==cpass){
          storeData()
        }else{
          Toast.show('password and confirm password not matching.')
        }
        
      }else{
        Toast.show('password not valid.')
      }
      
    }else{
      Toast.show('email not valid.')
    }
    
  }
  return (
    
     
<View>
  <View style={{alignItems:'center',marginVertical:10}}>
    <Text style={{}}>
      SignUp Screen
    </Text>
  </View>
      <InputBox
        placeholder="Email address"
        value={email}
        setValue={text => setemail(text.trim())}
      />
      <InputBox
        placeholder="Password"
        value={password}
        setValue={text => setpassword(text)}
        secureTextEntry
      />
       <InputBox
        placeholder="Confirm Password"
        value={cpass}
        setValue={text => setcpass(text)}
        secureTextEntry
      />
      <Button onPress={submit} title="Sign Up"></Button>
    </View>
     
  );
};

export default SignUp;
